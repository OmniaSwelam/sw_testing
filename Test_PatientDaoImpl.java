//import org.junit.Assert;

public class Test_PatientDaoImpl {
	@org.junit.Test
	public void constructorTest() throws PatientDaoException{
		Patient p= new Patient("Omnia","Mahmoud");
		p.setPhNum("01112375394");
		p.setSSN("1996534hgas2");
		
		PatientDaoImpl Imp= new PatientDaoImpl();

		Imp.insert_patient(p);
		
		/*Test passes if we only created instance of PatientDaoImpl (we will have an exception 
		 * that will be handled inside catch (printStackTrace) due to a problem in the connection to
		 * database.
		 * But if we tried to insert patient, the test will fail, because now we have a null connection.
		 */
	
	}
}
