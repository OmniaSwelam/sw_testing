import org.junit.Assert;

public class TestMerge {
	@org.junit.Test
	public void constructorTest() {
		int [] firstArr= {4,5,6};
		int [] secondArr= {1,2,3};	
		int [] thirdArr= {1,2,3};
		int [] fourthArr= {4,5,6};	
		
		int [] fifthArr= {};
		int [] sixthArr= {4,5,6};
		int [] seventhArr= {1,2,3};
		int [] eighthArr= null;
		int [] ninthArr= null;
		int [] tenthArr= null;
		
		int [] output1= MergeUniqueArrays.mergeArrays(firstArr, secondArr);
		int [] output2= MergeUniqueArrays.mergeArrays(thirdArr, fourthArr);
		int [] output3= MergeUniqueArrays.mergeArrays(fifthArr, sixthArr);
		int [] output4= MergeUniqueArrays.mergeArrays(seventhArr, eighthArr);
		int [] output5= MergeUniqueArrays.mergeArrays(ninthArr, tenthArr);
		
		
		int [] expected1= {1,2,3,4,5,6};
		int [] expected2= {1,2,3,4,5,6};
		int [] expected3= {4,5,6};
		int [] expected4= {1,2,3};
		int [] expected5= null;
		
		Assert.assertArrayEquals(expected1, output1);
		Assert.assertArrayEquals(expected2, output2);
		Assert.assertArrayEquals(expected3, output3);
		Assert.assertArrayEquals(expected4, output4);
		Assert.assertArrayEquals(expected5, output5);

}
}
