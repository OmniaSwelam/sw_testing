import org.junit.Assert;

public class Test {
@org.junit.Test 
public void constructorTest() {
	Patient p= new Patient("Ahmed", "Ali");
	Assert.assertTrue(p.getfname().equals("Ahmed")); 
	Assert.assertTrue(p.getlname().equals("Ali")); 
	
	p.setSSN("130sher23");
	Assert.assertTrue(p.getSSN().equals("130sher23"));
	
	p.setPhNum("01114593246");
	Assert.assertTrue(p.getPhNum().equals("01114593246"));
}
}
